﻿using System;

namespace Marsonsoft.ConfigSterilizer
{
	/// <summary>
	/// Base class for other Sterilizer classes providing common functionality.
	/// </summary>
	public abstract class SterilizerBase
	{
		private readonly ISterilizerSettings settings;

		/// <summary>
		/// Creates an instance of the <see cref="SterilizerBase"/> class that uses the specified <see cref="ISterilizerSettings"/>.
		/// </summary>
		/// <param name="settings">The sterilizer settings to use.</param>
		protected SterilizerBase(ISterilizerSettings settings)
		{
			if (settings == null)
			{
				throw new ArgumentNullException(nameof(settings));
			}
			this.settings = settings;
		}

		/// <summary>
		/// Gets the <see cref="ISterilizerSettings"/> of this instance.
		/// </summary>
		public ISterilizerSettings Settings
		{
			get { return settings; }
		}


	}
}
