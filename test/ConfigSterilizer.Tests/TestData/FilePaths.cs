﻿namespace Marsonsoft.ConfigSterilizer.Tests.TestData
{
	internal static class FilePaths
	{
		public const string MinimalConfig = "TestData\\MinimalConfigFile.xml";
		public const string ConnectionStrings = "TestData\\ConnectionStringsConfigFile.xml";
		public const string ConnectionStringsSterilized = "TestData\\ConnectionStringsConfigFileSterilized.xml";
		public const string AppSettings = "TestData\\AppSettingsConfigFile.xml";
		public const string AppSettingsSterilized = "TestData\\AppSettingsConfigFileSterilized.xml";
		public const string AppSettingsSterilizedWithComments = "TestData\\AppSettingsConfigFileSterilizedWithComments.xml";
		public const string Custom = "TestData\\CustomConfigFile.xml";
		public const string CustomSterilized = "TestData\\CustomConfigFileSterilized.xml";
		public const string CustomSection = "TestData\\ConfigFileWithCustomSection.xml";
		public const string CustomSectionSterilized = "TestData\\ConfigFileWithCustomSectionSterilized.xml";
		public const string SecretAttribute = "TestData\\ConfigFileWithSecretAttribute.xml";
		public const string SecretAttributeSterilized = "TestData\\ConfigFileWithSecretAttributeSterilized.xml";
		public const string SecretAttributeSterilizedNoHeuristics = "TestData\\ConfigFileWithSecretAttributeSterilizedNoHeuristics.xml";
		public const string NHibernate = "TestData\\ConfigFileWithNHibernate.xml";
		public const string NHibernateSterilized = "TestData\\ConfigFileWithNHibernateSterilized.xml";

	}
}
