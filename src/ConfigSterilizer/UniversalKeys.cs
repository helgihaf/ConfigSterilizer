﻿using System.Collections.Generic;

namespace Marsonsoft.ConfigSterilizer
{
	/// <summary>
	/// Universal keys for values that are considered as secret.
	/// </summary>
	public static class UniversalKeys
	{
		/// <summary>
		/// Full keys (whole words) whose values should be considered as secret.
		/// </summary>
		public static IEnumerable<string> FullNames { get; } = new[]
		{
			"pwd",
		};
			

		/// <summary>
		/// Partial keys (partial words, substrings) whose values should be considered as secret.
		/// </summary>
		public static IEnumerable<string> PartialNames { get; } = new[]
		{
			"pass",
			"pwd",
			"license",
			"encryptionkey",
			"apikey",
			"salt",
			"pin",
			"ptk",
			"credentials",
			"creditcard",
			"cardnum",
		};
	}
}
