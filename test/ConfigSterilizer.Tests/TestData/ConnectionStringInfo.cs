﻿namespace Marsonsoft.ConfigSterilizer.Tests.TestData
{
	internal class ConnectionStringInfo
	{
		public const string PasswordPlaceholder = "B333F6C29B514BDBB15A48B86F91918E";
		public string Base { get; set; }
		public string Password { get; set; }

		public string ConnectionString
		{
			get
			{
				return Base.Replace(PasswordPlaceholder, Password);
			}
		}

		public string Masked
		{
			get
			{
				return Base.Replace(PasswordPlaceholder, SterilizerSettings.DefaultMask);
			}
		}
	}
}
