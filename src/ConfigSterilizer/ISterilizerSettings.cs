﻿using System.Collections.Generic;
using System.Globalization;

namespace Marsonsoft.ConfigSterilizer
{
	/// <summary>
	/// Settings used to further specify the sterilization process.
	/// </summary>
	public interface ISterilizerSettings
	{
		/// <summary>
		/// Gets a value specifying whether, when scanning unknown data format, to use a heuristic
		/// technique to determine whether a value is a secret or not.
		/// </summary>
		/// <remarks>
		/// Known data formats include connection strings and certain known fixed key names in appSettings
		/// elements of config files.
		/// </remarks>
		bool UseHeuristics { get; }

		/// <summary>
		/// Gets a value specifying mask that the is used to replace sensitive data.
		/// </summary>
		string Mask { get; }

		/// <summary>
		/// Gets a value specifying whether to allow simple string formatting strings in values considered for masking or not.
		/// </summary>
		bool AllowSimpleFormatStringInValue { get; }

		/// <summary>
		/// Gets a value specifying whether comments are preserved while sterilizing.
		/// </summary>
		bool PreserveComments { get; }

		/// <summary>
		/// Gets a value specifying the CultureInfo to use in string comparisons.
		/// </summary>
		CultureInfo CultureInfo { get; }

		/// <summary>
		/// Full keys (whole words) whose values should be considered as secret.
		/// </summary>
		IEnumerable<string> FullKeys { get; }

		/// <summary>
		/// Partial keys (partial words, substrings) whose values should be considered as secret.
		/// </summary>
		IEnumerable<string> PartialKeys { get; }
	}
}