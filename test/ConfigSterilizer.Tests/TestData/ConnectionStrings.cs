﻿using System.Linq;

namespace Marsonsoft.ConfigSterilizer.Tests.TestData
{
	internal static class ConnectionStrings
	{
		public static readonly string[] Passwords = new string[]
		{
			"",
			"abc123",
			"myPassword",
			"<![CDATA[J&{#1&%?zz.,!'~@]]>",
			"{}",
			"{a}",
			"{1.3}",
			"{-1}",
			"{0,-2:C}",
		};

		// These are all strings that appear in a connectionString="" within an add element in a connectionStrings element.
		public readonly static string[] StandardConnectionStrings = new string[]
		{
			// -- Oracle
			"Data Source=eyjafjallajokull;User Id=plumes;Password=" + ConnectionStringInfo.PasswordPlaceholder + ";",
			"Data Source=eyjafjallajokull;User Id={0};Password={1};Pooling=false",
			"Data Source=eyjafjallajokull;User Id={0};Password=" + ConnectionStringInfo.PasswordPlaceholder + ";Pooling=false",
			"Data Source=eyjafjallajokull;User Id={0};Password=" + ConnectionStringInfo.PasswordPlaceholder + ";Pooling=false",
			"Data Source=eyjafjallajokull;User Id={0};Password=" + ConnectionStringInfo.PasswordPlaceholder + ";Pooling=false",
			"Data Source=eyjafjallajokull;User Id={0};Password=" + ConnectionStringInfo.PasswordPlaceholder + ";Pooling=false",
			"Data Source=eyjafjallajokull;User Id={0};Password=" + ConnectionStringInfo.PasswordPlaceholder + ";Pooling=false",
			"Data Source=eyjafjallajokull;uid=myUsername;pwd=" + ConnectionStringInfo.PasswordPlaceholder + ";",
			// -- SQL Server
			"Server=myServerAddress;Database=myDataBase;User Id=myUsername;Password=" + ConnectionStringInfo.PasswordPlaceholder + ";",
			"Server=myServerAddress;Database=myDataBase;Trusted_Connection=True;",
			"Server=myServerName\\myInstanceName; Database=myDataBase;User Id = myUsername; Password=" + ConnectionStringInfo.PasswordPlaceholder + ";",
			// -- MySql
			"Server=myServerAddress;Database=myDataBase;Uid=myUsername;Pwd=" + ConnectionStringInfo.PasswordPlaceholder + ";",
			"Server=myServerAddress;Database=myDataBase;Uid=myUsername;Pwd=" + ConnectionStringInfo.PasswordPlaceholder + ";SSL Mode=Required;CertificateFile=C:\\folder\\client.pfx;CertificatePassword=" + ConnectionStringInfo.PasswordPlaceholder + ";",
			// -- Access
			"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\\myFolder\\myAccessFile.accdb;Jet OLEDB:Database Password=" + ConnectionStringInfo.PasswordPlaceholder + ";",
			// -- PostgreSQL
			"User ID=root;Password=" + ConnectionStringInfo.PasswordPlaceholder + ";Host=localhost;Port=5432;Database=myDataBase;Pooling=true;Min Pool Size=0;Max Pool Size=100;Connection Lifetime=0;",
			"Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=w05125)(PORT=1521)))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=xe)));User Id=sr_usr;Password=" + ConnectionStringInfo.PasswordPlaceholder + ";",
			// -- Entity Framework
			"\r\n        metadata=res://*/MyDataModel.csdl|res://*/MyDataModel.ssdl|res://*/MyDataModel.msl;\r\n        provider=System.Data.SqlClient;\r\n        provider connection string=&quot;\r\n        Data Source=localhost\\DEV;\r\n        Initial Catalog=MyDB;UserId=myUser;\r\n        Password=" + ConnectionStringInfo.PasswordPlaceholder + ";\r\n        MultipleActiveResultSets=True&quot;\" \r\n        providerName=\"System.Data.EntityClient\"",
			// -- Nonsense but valid format
			"Data Source=adf",
		};

		// These are all strings that appear in a connectionString="" within an add element in a connectionStrings element.
		public static string[] OtherConnectionStrings = new string[]
		{
			// -- REST consumer
			"https://api.marsonsoft.com/InteractiveFiction/v1/",
		};

		// These strings should not be identified as standard connection strings
		public static string[] NotStandardConnectionStrings = new string[]
		{
			null,
			string.Empty,
			"  ",
			"Domino",
			"PasswordController",
			"Data Source",
		}.Concat(OtherConnectionStrings).ToArray();

		public static readonly string[] Bases = StandardConnectionStrings.Concat(OtherConnectionStrings).ToArray();
	}
}
