﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Marsonsoft.ConfigSterilizer.Tests
{
	[TestClass]
	public class SterilizerSettingsTest
	{
		[TestMethod]
		public void ShouldCreateSterilizerSettings()
		{
			var settings = new SterilizerSettings();
		}

		[TestMethod]
		public void ShouldHaveNonEmptyDefaultMask()
		{
			string mask = SterilizerSettings.DefaultMask;
			Assert.IsFalse(string.IsNullOrWhiteSpace(mask));
		}

		[TestMethod]
		public void ShouldHaveMaskAsDefaultMask()
		{
			var settings = new SterilizerSettings();
			Assert.AreEqual(SterilizerSettings.DefaultMask, settings.Mask);
		}

		[TestMethod]
		public void ShouldRememberMaskChange()
		{
			const string NewMask = "!";
			var settings = new SterilizerSettings { Mask = NewMask };
			Assert.AreEqual(NewMask, settings.Mask);
		}

		[TestMethod]
		public void ShouldAllowFormatAsDefault()
		{
			var settings = new SterilizerSettings();
			Assert.AreEqual(true, settings.AllowSimpleFormatStringInValue);
		}

		[TestMethod]
		public void ShouldRememberAllowFormatChange()
		{
			var settings = new SterilizerSettings { AllowSimpleFormatStringInValue = false };
			Assert.AreEqual(false, settings.AllowSimpleFormatStringInValue);
		}

		[TestMethod]
		public void ShouldRememberFullOverride()
		{
			const string NewMask = "!";
			var settings = new SterilizerSettings { Mask = NewMask, AllowSimpleFormatStringInValue = false };
			Assert.AreEqual(NewMask, settings.Mask);
			Assert.AreEqual(false, settings.AllowSimpleFormatStringInValue);
		}

		[TestMethod]
		public void ShouldCreateWithEmptyMask()
		{
			const string CustomMask = "";
			Assert.AreNotEqual(CustomMask, SterilizerSettings.DefaultMask);
			var settings = new SterilizerSettings { Mask = CustomMask };
			Assert.AreEqual(CustomMask, settings.Mask);
		}

		[TestMethod]
		public void ShouldCreateKeyValueSterilizerWithNullMask()
		{
			const string CustomMask = null;
			Assert.AreNotEqual(CustomMask, SterilizerSettings.DefaultMask);
			var settings = new SterilizerSettings { Mask = CustomMask };
			// Note: Mask may never be null, we assert that it has been fixed for us
			Assert.AreEqual(string.Empty, settings.Mask);
		}


	}
}
