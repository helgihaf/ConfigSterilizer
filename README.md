# ConfigSterilizer [![Build status](https://ci.appveyor.com/api/projects/status/s1j134yavvnv9frs?svg=true)](https://ci.appveyor.com/project/helgihaf/configsterilizer) [![NuGet](https://badge.fury.io/nu/Marsonsoft.ConfigSterilizer.svg)](https://www.nuget.org/packages/Marsonsoft.ConfigSterilizer) [![License](https://img.shields.io/github/license/mashape/apistatus.svg)](https://en.wikipedia.org/wiki/MIT_License)
## About

ConfigSterilizer is a small library for "sterilizing" config files through masking of passwords, API keys and other secret values. This enables you to write code that can display of configuration and settings without revealing secrets. This can be useful in anti-devops environment where walls exist between developers and operations.

## License

The project is licensed under the MIT license.

## Quickstart

In its simplest form you can sterilize a config file like this:
```C#
string sterilized = Sterilizer.SterilizeConfigFile(@"..\..\App.config");
```

If more customization is needed use the following:
```C#
SterilizerSettings settings = new SterilizerSettings
{
    CultureInfo = new System.Globalization.CultureInfo("is-IS"),
    Mask = "*",
    PreserveComments = true
};
settings.PartialKeys.Add("lykilorð");
XDocument xdoc = XDocument.Load(@"..\..\App.config");
XSterilizer sterilizer = new XSterilizer(settings);
XDocument sterilizedXdoc = sterilizer.Sterilize(xdoc);
```

In the event you must enforce a secret without changing code you can apply attributes to your config elements to instruct the Sterilizer to sterize an attribute value or to hide the contents of an entire element:
```XML
<?xml version="1.0" encoding="utf-8" ?>
<configuration xmlns:msec="http://schemas.marsonsoft.com/sterilizer/secrets">
  <appSettings>
    <add key="HiddenMessage" value="the eagle has landed" msec:Attributes="value"/>
  </appSettings>
  <someElement>
    <secretElement msec:Element="true">
      The eagle has landed
    </secretElement>
  </someElement>
</configuration>
```
This will be sterilized as:
```XML
<?xml version="1.0" encoding="utf-8" ?>
<configuration xmlns:msec="http://schemas.marsonsoft.com/sterilizer/secrets">
  <appSettings>
    <add key="HiddenMessage" value="???" />
  </appSettings>
  <someElement />
</configuration>
```
