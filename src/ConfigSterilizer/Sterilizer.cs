﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Marsonsoft.ConfigSterilizer
{
	/// <summary>
	/// Contains method for simple sterilization of config files and connection strings.
	/// </summary>
	public static class Sterilizer
	{
		/// <summary>
		/// Sterilizes the specified config file using the default <see cref="SterilizerSettings"/>.
		/// </summary>
		/// <param name="sourceFilePath">Path to the the config file to be sterilized.</param>
		/// <returns>The sterilized contents of the config file.</returns>
		public static string SterilizeConfigFile(string sourceFilePath)
		{
			return SterilizeConfigFile(sourceFilePath, new SterilizerSettings());
		}

		/// <summary>
		/// Sterilizes the specified config file using the specifid settings.
		/// </summary>
		/// <param name="sourceFilePath">Path to the the config file to be sterilized.</param>
		/// <param name="settings">Settings to use for ther sterilization process.</param>
		/// <returns>The sterilized contents of the config file.</returns>
		public static string SterilizeConfigFile(string sourceFilePath, ISterilizerSettings settings)
		{
			var xdoc = XDocument.Load(sourceFilePath);
			var sterilizer = new XSterilizer(settings);
			var sterilizedXdoc = sterilizer.Sterilize(xdoc);
			return sterilizedXdoc.ToString();
		}

		/// <summary>
		/// Sterilizes a config contained in an XML document using the default <see cref="SterilizerSettings"/>.
		/// </summary>
		/// <param name="xdoc">An XML document containing the config to sterilize.</param>
		/// <returns>A sterilized version of <paramref name="xdoc"/></returns>
		public static XDocument SterilizeConfig(XDocument xdoc)
		{
			return SterilizeConfig(xdoc, new SterilizerSettings());
		}

		/// <summary>
		/// Sterilizes a config contained in an XML document using the specified settings.
		/// </summary>
		/// <param name="xdoc">An XML document containing the config to sterilize.</param>
		/// <param name="settings">Settings to use for ther sterilization process.</param>
		/// <returns>A sterilized version of <paramref name="xdoc"/></returns>
		public static XDocument SterilizeConfig(XDocument xdoc, ISterilizerSettings settings)
		{
			var sterilizer = new XSterilizer(settings);
			return sterilizer.Sterilize(xdoc);
		}

		/// <summary>
		/// Sterilizes the specified configuration string using the default <see cref="SterilizerSettings"/>.
		/// </summary>
		/// <param name="text">The configuration string to sterilize.</param>
		/// <returns>A sterilized version of <paramref name="text"/></returns>
		public static string SterilizeConfig(string text)
		{
			return SterilizeConfig(text, new SterilizerSettings());
		}

		/// <summary>
		/// Sterilizes the specified configuration string using the specified settings.
		/// </summary>
		/// <param name="text">The configuration string to sterilize.</param>
		/// <param name="settings">Settings to use for ther sterilization process.</param>
		/// <returns>A sterilized version of <paramref name="text"/></returns>
		public static string SterilizeConfig(string text, ISterilizerSettings settings)
		{
			var xdoc = XDocument.Parse(text);
			var sterilizer = new XSterilizer(settings);
			var sterilizedXdoc = sterilizer.Sterilize(xdoc);
			return sterilizedXdoc.ToString();
		}

		/// <summary>
		/// Sterilizes a connection string using the default <see cref="SterilizerSettings"/>.
		/// </summary>
		/// <param name="connectionString">The connection string to sterilize.</param>
		/// <returns>A sterilized version of <paramref name="connectionString"/></returns>
		public static string SterilizeConnectionString(string connectionString)
		{
			return SterilizeConnectionString(connectionString, new SterilizerSettings());
		}

		/// <summary>
		/// Sterilizes a connection string using the specified settings.
		/// </summary>
		/// <param name="connectionString">The connection string to sterilize.</param>
		/// <param name="settings">Settings to use for ther sterilization process.</param>
		/// <returns>A sterilized version of <paramref name="connectionString"/></returns>
		public static string SterilizeConnectionString(string connectionString, ISterilizerSettings settings)
		{
			var sterilizer = new ConnectionStringSterilizer(settings);
			return sterilizer.Sterilize(connectionString);
		}
	}
}
