﻿namespace Marsonsoft.ConfigSterilizer.Tests.TestData
{
	internal static class SecretKeys
	{
		public static readonly string[] BasicPasswordKeys = new[]
		{
			"password",
			"PASSWORD",
			"Password",
		};


		public static readonly string[] CommonKeys = new[]
		{
			"Password",
			"userPassword",
			"PaymentPassword",
			"pwd",
			"SecureProductLicenseKey",
		};

		public static readonly string[] NonPasswordKeys = new[]
		{
			"User Id",
			"Data Source",
			"serviceUrl",
			"logFile",
			"username",
			"ssn",
		};

		public static readonly string[] ApiKeyKeys = new[]
		{
			"apikey",
			"ApiKey",
			"APIKEY",
			"MyApiKey",
			"ApiKeyService",
		};

	}
}
