﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Marsonsoft.ConfigSterilizer
{
	/// <summary>
	/// Settings shared by one or more Sterilizers classes.
	/// </summary>
	public class SterilizerSettings : ISterilizerSettings
	{
		/// <summary>
		/// The default mask to place over sensitive data.
		/// </summary>
		public const string DefaultMask = "???";

		private string mask = DefaultMask;

		/// <summary>
		/// Creates a new instance of the <see cref="SterilizerSettings"/> class.
		/// </summary>
		public SterilizerSettings()
		{
			// Add standard, universal keys to our collection.
			((List<string>)FullKeys).AddRange(UniversalKeys.FullNames);
			((List<string>)PartialKeys).AddRange(UniversalKeys.PartialNames);
		}

		/// <summary>
		/// Gets or sets a value specifying whether, when scanning unknown data format, to use a heuristic
		/// technique to determine whether a value is a secret or not. Default true.
		/// </summary>
		/// <remarks>
		/// Known data formats include connection strings and certain known fixed key names in appSettings
		/// elements of config files.
		/// </remarks>
		public bool UseHeuristics { get; set; } = true;

		/// <summary>
		/// Gets or sets a value specifying mask that the class uses to replace sensitive data.
		/// </summary>
		public string Mask
		{
			get { return mask; }
			set	{ mask = value ?? string.Empty;	}
		}

		/// <summary>
		/// Gets or sets a value specifying whether to allow simple string formatting strings in values or not.
		/// Default true.
		/// </summary>
		public bool AllowSimpleFormatStringInValue { get; set; } = true;

		/// <summary>
		/// Gets or sets a value specifying whether comments are preserved while sterilizing.
		/// </summary>
		public bool PreserveComments { get; set; } = false;

		/// <summary>
		/// Gets or sets a value specifying the CultureInfo to use in string comparisons.
		/// Default is CultureInfo.CurrentCulture.
		/// </summary>
		public CultureInfo CultureInfo { get; set; } = CultureInfo.CurrentCulture;

		/// <summary>
		/// List of full keys (whole words) whose values should be considered as secret.
		/// </summary>
		public IList<string> FullKeys { get; } = new List<string>();

		/// <summary>
		/// List of partial keys (partial words, substrings) whose values should be considered as secret.
		/// </summary>
		public IList<string> PartialKeys { get; } = new List<string>();

		IEnumerable<string> ISterilizerSettings.FullKeys
		{
			get
			{
				return FullKeys;
			}
		}

		IEnumerable<string> ISterilizerSettings.PartialKeys
		{
			get
			{
				return PartialKeys;
			}
		}
	}
}
