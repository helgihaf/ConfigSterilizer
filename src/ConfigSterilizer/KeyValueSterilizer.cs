﻿using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Marsonsoft.ConfigSterilizer
{
	/// <summary>
	/// Sterilizes values in a key-value pair.
	/// </summary>
	public class KeyValueSterilizer : SterilizerBase
	{
		// Full syntax of .NET format strings is:
		//	{ index[, alignment][ :formatString] }
		// But for the sake of masking passwords we only allow
		//  { index }
		private static Regex formatStringRegex = new Regex(@"^\{\d+}$");

		/// <summary>
		/// Creates an instance of the <see cref="KeyValueSterilizer"/> class that uses the specified <see cref="ISterilizerSettings"/>.
		/// </summary>
		/// <param name="settings">The sterilizer settings to use.</param>
		public KeyValueSterilizer(ISterilizerSettings settings)
			: base(settings)
		{
		}

		/// <summary>
		/// Determines if the value of the specified key/value combination should be masked.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		/// <returns><c>true</c> if the value should be masked, <c>false</c> otherwise.</returns>
		public bool ShouldMaskKeyValue(string key, string value)
		{
			return 
				(key != null) 
				&& IsSecretKey(key)
				&& (!Settings.AllowSimpleFormatStringInValue || !IsSimpleFormatString(value));
		}

		/// <summary>
		/// Sterilizes the value of the specified key/value combination.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		/// <returns>The sterilized value. That is, either the original value or the Mask.</returns>
		/// <seealso cref="TrySterilizeKeyValue(string, string, out string)"/>
		public string SterilizeKeyValue(string key, string value)
		{
			string result = value;
			if (ShouldMaskKeyValue(key, value))
			{
				result = Settings.Mask;
			}

			return result;
		}

		/// <summary>
		/// Tries to sterilize the value of the specified key/value combination.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="value">The value.</param>
		/// <param name="sterilizedValue">If the function returned <c>true</c> this is the sterilized value.</param>
		/// <returns><c>true</c> if the value was sterilized, <c>false</c> otherwise.</returns>
		public bool TrySterilizeKeyValue(string key, string value, out string sterilizedValue)
		{
			bool sterilized = false;
			sterilizedValue = null;
			if (ShouldMaskKeyValue(key, value))
			{
				sterilizedValue = Settings.Mask;
				sterilized = true;
			}
			return sterilized;
		}

		/// <summary>
		/// Determines if the specified key is likely to belong to a secret (password, api key, license key,...).
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns><c>true</c> if the specified key is likely to belong to a secret, <c>false</c> otherwise.</returns>
		public bool IsSecretKey(string key)
		{
			return PartialMatch(key) || FullMatch(key);
		}

		private bool PartialMatch(string key)
		{
			return Settings.PartialKeys.Any(p => Settings.CultureInfo.CompareInfo.IndexOf(key, p, CompareOptions.IgnoreCase) >= 0);
		}

		private bool FullMatch(string key)
		{
			return Settings.FullKeys.Any(p => Settings.CultureInfo.CompareInfo.Compare(key, p, CompareOptions.IgnoreCase) == 0);
		}

		private static bool IsSimpleFormatString(string s)
		{
			return (s != null) && formatStringRegex.IsMatch(s);
		}
	}
}
