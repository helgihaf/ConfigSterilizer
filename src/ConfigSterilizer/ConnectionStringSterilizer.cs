﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Marsonsoft.ConfigSterilizer
{
	/// <summary>
	/// Sterilizes standard connection strings.
	/// </summary>
	public class ConnectionStringSterilizer : SterilizerBase
	{
		private readonly KeyValueSterilizer keyValueSterilizer;

		/// <summary>
		/// Determines if the specified string is a standard connection string.
		/// </summary>
		/// <param name="s">The string to check.</param>
		/// <returns>True if the specified string is a standard connection string, false otherwize.</returns>
		public static bool IsStandardConnectionString(string s)
		{
			var list = ParseConnectionString(s);
			if (list == null || list.Count == 0 || (list.Count == 1 && list[0].Value == null))
			{
				return false;
			}
			return list.Any(p => IsConnectionStringKey(p.Key));
		}

		private static bool IsConnectionStringKey(string key)
		{
			if (string.IsNullOrWhiteSpace(key))
			{
				return false;
			}

			return
				key.IndexOf("Data Source", StringComparison.OrdinalIgnoreCase) >= 0
				|| key.IndexOf("Server", StringComparison.OrdinalIgnoreCase) >= 0
				|| key.IndexOf("Database", StringComparison.OrdinalIgnoreCase) >= 0
				|| key.IndexOf("Password", StringComparison.OrdinalIgnoreCase) >= 0
				;
		}

		/// <summary>
		/// Creates an instance of the <see cref="ConnectionStringSterilizer"/> class that uses the specified <see cref="ISterilizerSettings"/>.
		/// </summary>
		/// <param name="settings">The sterilizer settings to use.</param>
		public ConnectionStringSterilizer(ISterilizerSettings settings)
			: base(settings)
		{
			this.keyValueSterilizer = new KeyValueSterilizer(Settings);
		}

		/// <summary>
		/// Creates an instance of the <see cref="ConnectionStringSterilizer"/> class that uses the specified <see cref="KeyValueSterilizer"/>
		/// and its settings.
		/// </summary>
		/// <param name="keyValueSterilizer">The key value serializer class to use when sterilizing connection strings.</param>
		public ConnectionStringSterilizer(KeyValueSterilizer keyValueSterilizer)
			: base(keyValueSterilizer?.Settings)
		{
			this.keyValueSterilizer = keyValueSterilizer;
		}

		/// <summary>
		/// Sterilizes the specified connection string.
		/// </summary>
		/// <param name="connectionString">A standard connection string.</param>
		/// <returns>A sterilized version of the connection string.</returns>
		public string Sterilize(string connectionString)
		{
			var list = ParseConnectionString(connectionString);
			if (list == null || list.Count == 0)
			{
				return connectionString;
			}

			for (int i = 0; i < list.Count; i++)
			{
				string sterilizedValue;
				if (keyValueSterilizer.TrySterilizeKeyValue(list[i].Key, list[i].Value, out sterilizedValue))
				{
					list[i] = new KeyValuePair<string, string>(list[i].Key, sterilizedValue);
				}
			}

			return ToConnectionString(list);
		}

		private static IList<KeyValuePair<string, string>> ParseConnectionString(string connectionString)
		{
			if (string.IsNullOrEmpty(connectionString))
			{
				return null;
			}

			string[] assignments = connectionString.Split(';');

			var result = new List<KeyValuePair<string, string>>();
			for (int i = 0; i < assignments.Length; i++)
			{
				string assignment = assignments[i];
				string[] parts = assignment.Split('=');
				if (parts.Length == 2)
				{
					result.Add(new KeyValuePair<string, string>(parts[0], parts[1]));
				}
				else
				{
					result.Add(new KeyValuePair<string, string>(assignment, null));
				}
			}
			return result;
		}

		private static string ToConnectionString(IList<KeyValuePair<string, string>> list)
		{
			return string.Join(";", list.Select(p => p.Key + (p.Value != null ? ("=" + p.Value) : string.Empty)));
		}


	}
}
