﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Xml.Linq;

namespace Marsonsoft.ConfigSterilizer.Tests
{
	[TestClass]
	public class XSterilizerTests
	{
		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ShouldThrowExceptionOnNullSettings()
		{
			new XSterilizer(null);
		}

		[TestMethod]
		public void ShouldLeaveConfigFileAlone()
		{
			var sterilizer = new XSterilizer(new SterilizerSettings());
			var source = XDocument.Load(TestData.FilePaths.MinimalConfig);
			var target = sterilizer.Sterilize(source);
			Assert.IsFalse(object.ReferenceEquals(source, target));
			XUtils.AssertAreEqual(source, target);
		}

		[TestMethod]
		public void ShouldSterilizeConfigWithConnectionString()
		{
			var sterilizer = new XSterilizer(new SterilizerSettings());
			var source = XDocument.Load(TestData.FilePaths.ConnectionStrings);
			var target = sterilizer.Sterilize(source);
			var expected = XDocument.Load(TestData.FilePaths.ConnectionStringsSterilized);
			XUtils.AssertAreEqual(expected, target);
		}

		[TestMethod]
		public void ShouldSterilizeConfigWithAppSettings()
		{
			var sterilizer = new XSterilizer(new SterilizerSettings());
			var source = XDocument.Load(TestData.FilePaths.AppSettings);
			var target = sterilizer.Sterilize(source);
			var expected = XDocument.Load(TestData.FilePaths.AppSettingsSterilized);
			XUtils.AssertAreEqual(expected, target);
		}

		[TestMethod]
		public void ShouldSterilizeCustomConfig()
		{
			var sterilizer = new XSterilizer(new SterilizerSettings());
			var source = XDocument.Load(TestData.FilePaths.Custom);
			var target = sterilizer.Sterilize(source);
			var expected = XDocument.Load(TestData.FilePaths.CustomSterilized);
			XUtils.AssertAreEqual(expected, target);
		}


		[TestMethod]
		public void ShouldSterilizeWithCustomMask()
		{
			var settings = new SterilizerSettings();
			settings.Mask = "!";
			var sterilizer = new XSterilizer(settings);
			var source = XDocument.Load(TestData.FilePaths.ConnectionStrings);

			var target = sterilizer.Sterilize(source);

			var expected = XUtils.Normalize(XDocument.Load(TestData.FilePaths.ConnectionStringsSterilized)).Replace(SterilizerSettings.DefaultMask, settings.Mask);
			var actual = XUtils.Normalize(target);
			Assert.AreEqual(expected, actual);
		}


		[TestMethod]
		public void ShouldSterilizeConfigWithAppSettingsAndPreserveComments()
		{
			var settings = new SterilizerSettings();
			settings.PreserveComments = true;
			var sterilizer = new XSterilizer(settings);
			var source = XDocument.Load(TestData.FilePaths.AppSettings);
			var target = sterilizer.Sterilize(source);
			var expected = XDocument.Load(TestData.FilePaths.AppSettingsSterilizedWithComments);
			XUtils.AssertAreEqual(expected, target);
		}

		[TestMethod]
		public void ShouldSterilizeConfigWithCustomSection()
		{
			var sterilizer = new XSterilizer(new SterilizerSettings());
			var source = XDocument.Load(TestData.FilePaths.CustomSection);
			var target = sterilizer.Sterilize(source);
			var expected = XDocument.Load(TestData.FilePaths.CustomSectionSterilized);
			XUtils.AssertAreEqual(expected, target);
		}

		[TestMethod]
		public void ShouldSterilizeConfigWithSecretAttribute()
		{
			var sterilizer = new XSterilizer(new SterilizerSettings());
			var source = XDocument.Load(TestData.FilePaths.SecretAttribute);
			var target = sterilizer.Sterilize(source);
			var expected = XDocument.Load(TestData.FilePaths.SecretAttributeSterilized);
			XUtils.AssertAreEqual(expected, target);
		}

		[TestMethod]
		public void ShouldSterilizeConfigWithSecretAttributeAndNoHeuristics()
		{
			var settings = new SterilizerSettings();
			settings.UseHeuristics = false;
			var sterilizer = new XSterilizer(settings);
			var source = XDocument.Load(TestData.FilePaths.SecretAttribute);
			var target = sterilizer.Sterilize(source);
			var expected = XDocument.Load(TestData.FilePaths.SecretAttributeSterilizedNoHeuristics);
			XUtils.AssertAreEqual(expected, target);
		}

		[TestMethod]
		public void ShouldSterilizeConfigWithNHibernate()
		{
			var sterilizer = new XSterilizer(new SterilizerSettings());
			var source = XDocument.Load(TestData.FilePaths.NHibernate);
			var target = sterilizer.Sterilize(source);
			var expected = XDocument.Load(TestData.FilePaths.NHibernateSterilized);
			XUtils.AssertAreEqual(expected, target);
		}
	}
}
