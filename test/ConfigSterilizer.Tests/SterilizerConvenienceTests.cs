﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Marsonsoft.ConfigSterilizer.Tests
{
	[TestClass]
	public class SterilizerConvenienceTests
	{
		private readonly TestData.ConnectionStringInfo connectionStringInfo = new TestData.ConnectionStringInfo
		{
			Base = TestData.ConnectionStrings.Bases[0],
			Password = TestData.ConnectionStrings.Passwords[0],
		};

		[TestMethod]
		public void ShouldSterilizeConfigFile()
		{
			string sterilized = Sterilizer.SterilizeConfigFile(TestData.FilePaths.AppSettings);
			string expected = XUtils.NormalizeFile(TestData.FilePaths.AppSettingsSterilized);
			XUtils.AssertAreEqual(expected, XUtils.Normalize(sterilized));
		}

		[TestMethod]
		public void ShouldSterilizeConfigFileWithSetup()
		{
			var settings = new SterilizerSettings();
			string sterilized = Sterilizer.SterilizeConfigFile(TestData.FilePaths.AppSettings, settings);
			string expected = XUtils.NormalizeFile(TestData.FilePaths.AppSettingsSterilized);
			XUtils.AssertAreEqual(expected, XUtils.Normalize(sterilized));
		}

		[TestMethod]
		public void ShouldSterilizeConfigXDocument()
		{
			XDocument sterilized = Sterilizer.SterilizeConfig(XDocument.Load(TestData.FilePaths.AppSettings));
			XDocument expected = XDocument.Load(TestData.FilePaths.AppSettingsSterilized);
			XUtils.AssertAreEqual(expected, sterilized);
		}

		[TestMethod]
		public void ShouldSterilizeConfigXDocumentWithSettings()
		{
			XDocument sterilized = Sterilizer.SterilizeConfig(XDocument.Load(TestData.FilePaths.AppSettings), new SterilizerSettings());
			XDocument expected = XDocument.Load(TestData.FilePaths.AppSettingsSterilized);
			XUtils.AssertAreEqual(expected, sterilized);
		}

		[TestMethod]
		public void ShouldSterilizeConfigString()
		{
			string config = File.ReadAllText(TestData.FilePaths.AppSettings);
			string sterilized = Sterilizer.SterilizeConfig(config);
			string expected = XUtils.Normalize(File.ReadAllText(TestData.FilePaths.AppSettingsSterilized));
			XUtils.AssertAreEqual(expected, XUtils.Normalize(sterilized));
		}

		[TestMethod]
		public void ShouldSterilizeConfigStringWithSettings()
		{
			string config = File.ReadAllText(TestData.FilePaths.AppSettings);
			string sterilized = Sterilizer.SterilizeConfig(config, new SterilizerSettings());
			string expected = XUtils.Normalize(File.ReadAllText(TestData.FilePaths.AppSettingsSterilized));
			XUtils.AssertAreEqual(expected, XUtils.Normalize(sterilized));
		}

		[TestMethod]
		public void ShouldSterilizeConnectionString()
		{
			var source = connectionStringInfo.ConnectionString;
			var actual = Sterilizer.SterilizeConnectionString(source);
			var expected = connectionStringInfo.Masked;
			Assert.AreEqual(expected, actual);
		}

		[TestMethod]
		public void ShouldSterilizeConnectionStringWithSettings()
		{
			var source = connectionStringInfo.ConnectionString;
			var actual = Sterilizer.SterilizeConnectionString(source, new SterilizerSettings());
			var expected = connectionStringInfo.Masked;
			Assert.AreEqual(expected, actual);
		}
	}
}
