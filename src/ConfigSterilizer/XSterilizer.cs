﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Marsonsoft.ConfigSterilizer
{
	/// <summary>
	/// Contains methods to sterilize XML configuration files.
	/// </summary>
	public class XSterilizer : SterilizerBase
	{
		/// <summary>
		/// The XML namespace for the attributes used to mark data as a secret.
		/// </summary>
		public const string XmlNamespace = "http://schemas.marsonsoft.com/sterilizer/secrets";

		private const string StandardConnectionStringsElementName = "connectionStrings";
		private const string OldConnectionStringElementName = "connectionstring";
		private const string StandardAppSettingsElementName = "appSettings";

		private readonly XName secretElementAttributeName = XName.Get("Element", XmlNamespace);
		private readonly XName secretAttributesAttributeName = XName.Get("Attributes", XmlNamespace);
		private readonly Lazy<KeyValueSterilizer> lazyKeyValueSterilizer;
		private readonly Lazy<ConnectionStringSterilizer> lazyConnectionStringSterilizer;

		/// <summary>
		/// Creates an instance of the <see cref="XSterilizer"/> class that uses the specified <see cref="ISterilizerSettings"/>.
		/// </summary>
		/// <param name="settings">The sterilizer settings to use.</param>
		public XSterilizer(ISterilizerSettings settings)
			: base(settings)
		{
			lazyKeyValueSterilizer = new Lazy<KeyValueSterilizer>(() => new KeyValueSterilizer(settings));
			lazyConnectionStringSterilizer = new Lazy<ConnectionStringSterilizer>(() => new ConnectionStringSterilizer(lazyKeyValueSterilizer.Value));
		}

		/// <summary>
		/// Accepts an XML document and returnes a sterilized version of it.
		/// </summary>
		/// <param name="source">The XML document to sterilize.</param>
		/// <returns>A sterilized version of the document.</returns>
		public XDocument Sterilize(XDocument source)
		{
			var target = new XDocument(source);
			foreach (var node in target.Nodes().ToList())
			{
				SterilizeNode(node);
			}
			return target;
		}

		private void SterilizeNode(XNode node)
		{
			XElement element;

			if ((element = node as XElement) != null)
			{
				SterilizeElement(element);
			}
			else if (node is XComment && node.Parent != null && !Settings.PreserveComments)
			{
				node.Remove();
			}

			var container = node as XContainer;
			if (container == null)
			{
				return;
			}

			foreach (var child in container.Nodes().ToList())
			{
				SterilizeNode(child);
			}
		}

		private void SterilizeElement(XElement element)
		{
			if (IsSecretElement(element))
			{
				element.Remove();
				return;
			}

			if (SterilizeKnownDeterministicElement(element))
			{
				return;
			}

			if (SterilizeWellKnownElement(element))
			{
				return;
			}

			SterilizeGenericElement(element);
		}

		/// <summary>
		/// Sterilizes elements where we know where the secrets are. No heuristics or secret marker attributes needed.
		/// </summary>
		private bool SterilizeKnownDeterministicElement(XElement element)
		{
			if (StandardConnectionStringsElementName.Equals(element.Name.LocalName, StringComparison.Ordinal))
			{
				if (ForEachAddElementIn(element, add => SterilizeConnectionStringElement(add)))
				{
					return true;
				}
			}

			if (OldConnectionStringElementName.Equals(element.Name.LocalName, StringComparison.OrdinalIgnoreCase)
				&& !element.HasAttributes && element.Value != null)
			{
				element.Value = lazyConnectionStringSterilizer.Value.Sterilize(element.Value);
				return true;
			}

			return false;
		}

		/// <summary>
		/// Sterilizes elements where we know the structure but we still need heuristics or secret marker attributes.
		/// </summary>
		private bool SterilizeWellKnownElement(XElement element)
		{
			if (StandardAppSettingsElementName.Equals(element.Name.LocalName, StringComparison.Ordinal))
			{
				if (ForEachAddElementIn(element, add => SterilizeAppSettingsElement(add)))
				{
					return true;
				}
			}

			return false;
		}

		private void SterilizeGenericElement(XElement element)
		{
			SterilizeAttributes(element);
			if (Settings.UseHeuristics && !element.IsEmpty && !element.HasElements)
			{
				SterilizeElementValue(element);
			}
		}

		private bool IsSecretElement(XElement element)
		{
			return
				string.Equals("true", element.Attribute(secretElementAttributeName)?.Value, StringComparison.OrdinalIgnoreCase);
		}

		private static bool ForEachAddElementIn(XElement element, Action<XElement> actionOnAddElement)
		{
			bool hasAddElements = false;

			foreach (var addElement in element.Elements("add"))
			{
				actionOnAddElement(addElement);
				hasAddElements = true;
			}

			return hasAddElements;
		}

		private void SterilizeConnectionStringElement(XElement addElement)
		{
			var connectionString = addElement.Attribute("connectionString");
			if (connectionString == null)
			{
				return;
			}
			connectionString.Value = lazyConnectionStringSterilizer.Value.Sterilize(connectionString.Value);
		}

		private void SterilizeAppSettingsElement(XElement addElement)
		{
			if (!addElement.HasAttributes)
			{
				return;
			}

			foreach (var attribute in GetSecretAttributes(addElement))
			{
				attribute.Value = Settings.Mask;
			}

			if (!Settings.UseHeuristics)
			{
				return;
			}

			var key = addElement.Attribute("key")?.Value;
			TrySterilizeKeyValueElement(addElement, key);
		}

		private void SterilizeAttributes(XElement element)
		{
			if (!element.HasAttributes)
			{
				return;
			}

			foreach (var attribute in GetSecretAttributes(element))
			{
				attribute.Value = Settings.Mask;
			}

			if (!Settings.UseHeuristics)
			{
				return;
			}

			if (!TrySterilizeKeyValueElement(element))
			{
				foreach (var attribute in element.Attributes())
				{
					SterilizeAttribute(attribute);
				}
			}
		}

		private void SterilizeElementValue(XElement element)
		{
			if (!string.IsNullOrWhiteSpace(element.Value))
			{
				string sterilizedValue;
				if (lazyKeyValueSterilizer.Value.TrySterilizeKeyValue(element.Name.LocalName, element.Value, out sterilizedValue))
				{
					element.Value = sterilizedValue;
					return;
				}

				if (ConnectionStringSterilizer.IsStandardConnectionString(element.Value))
				{
					element.Value = lazyConnectionStringSterilizer.Value.Sterilize(element.Value);
				}
			}
		}

		private IEnumerable<XAttribute> GetSecretAttributes(XElement element)
		{
			var attribute = element.Attribute(secretAttributesAttributeName);
			if (attribute == null)
			{
				return new XAttribute[0];
			}
			string attributeNames = attribute.Value;
			attribute.Remove();
			if (string.IsNullOrWhiteSpace(attributeNames))
			{
				return new XAttribute[0];
			}
			string[] names = attributeNames.Split(',', ';');
			return element.Attributes().Where(a => names.Contains(a.Name.LocalName));
		}

		private bool TrySterilizeKeyValueElement(XElement element)
		{
			string key = GetKeyAttribute(element);
			return TrySterilizeKeyValueElement(element, key);
		}

		private bool TrySterilizeKeyValueElement(XElement element, string key)
		{
			var valueAttribute = element.Attribute("value");
			if (key == null || valueAttribute == null)
			{
				return false;
			}

			string sterilizedValue;
			bool sterilized = lazyKeyValueSterilizer.Value.TrySterilizeKeyValue(key, valueAttribute.Value, out sterilizedValue);
			if (sterilized)
			{
				valueAttribute.Value = sterilizedValue;
			}

			return sterilized;
		}

		private void SterilizeAttribute(XAttribute attribute)
		{
			string sterilizedValue;
			if (lazyKeyValueSterilizer.Value.TrySterilizeKeyValue(attribute.Name.LocalName, attribute.Value, out sterilizedValue))
			{
				attribute.Value = sterilizedValue;
			}

			if (ConnectionStringSterilizer.IsStandardConnectionString(attribute.Value))
			{
				attribute.Value = lazyConnectionStringSterilizer.Value.Sterilize(attribute.Value);
			}
		}

		private static string GetKeyAttribute(XElement element)
		{
			string key = element.Attribute("key")?.Value;
			if (key == null)
			{
				key = element.Attribute("name")?.Value;
			}
			return key;
		}
	}
}
