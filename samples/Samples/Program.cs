﻿using Marsonsoft.ConfigSterilizer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Samples
{
	class Program
	{
		static void Main(string[] args)
		{
			SimpleSterilize();
			SimpleSterilizeWithSettings();
			SterilizeMany();

			Console.WriteLine("All done, press any key to exit");
			Console.ReadKey();
		}

		private static void SimpleSterilize()
		{
			string sterilized = Sterilizer.SterilizeConfigFile(@"..\..\App.config");
			Console.WriteLine(sterilized);
		}

		private static void SimpleSterilizeWithSettings()
		{
			var settings = new SterilizerSettings
			{
				CultureInfo = new System.Globalization.CultureInfo("is-IS"),
				Mask = "*",
				PreserveComments = true
			};
			string sterilized = Sterilizer.SterilizeConfigFile(@"..\..\App.config", settings);
			Console.WriteLine(sterilized);
		}

		private static void SterilizeMany()
		{
			var settings = new SterilizerSettings();
			var sterilizer = new XSterilizer(settings);
			foreach (var configFilePath in GetConfigFiles())
			{
				var xdoc = XDocument.Load(configFilePath);
				var sterilizedXdoc = sterilizer.Sterilize(xdoc);
				Console.WriteLine(sterilizedXdoc.ToString());
			}
		}

		private static void FullSterilizeWithAdvancedSettings()
		{
			SterilizerSettings settings = new SterilizerSettings
			{
				CultureInfo = new System.Globalization.CultureInfo("is-IS"),
				Mask = "*",
				PreserveComments = true
			};
			settings.PartialKeys.Add("lykilorð");
			XDocument xdoc = XDocument.Load(@"..\..\App.config");
			XSterilizer sterilizer = new XSterilizer(settings);
			XDocument sterilizedXdoc = sterilizer.Sterilize(xdoc);
		}

		private static IEnumerable<string> GetConfigFiles()
		{
			yield return @"..\..\App.config";
		}
	}
}
