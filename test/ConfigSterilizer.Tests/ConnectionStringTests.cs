﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System;

namespace Marsonsoft.ConfigSterilizer.Tests
{
	[TestClass]
	public class ConnectionStringTests
	{
		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ShouldThrowExceptionOnNullSettings()
		{
			ISterilizerSettings settings = null;
			new ConnectionStringSterilizer(settings);
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ShouldThrowExceptionOnNullKeyValueSterilizer()
		{
			KeyValueSterilizer kvs = null;
			new ConnectionStringSterilizer(kvs);
		}

		[TestMethod]
		public void ShouldTestMasking()
		{
			var sterilizer = new ConnectionStringSterilizer(new SterilizerSettings());
			foreach (var connectionStringBase in TestData.ConnectionStrings.Bases)
			{
				foreach (var password in TestData.ConnectionStrings.Passwords)
				{
					var connectionStringInfo = new TestData.ConnectionStringInfo
					{
						Base = connectionStringBase,
						Password = password,
					};
					var source = connectionStringInfo.ConnectionString;
					var actual = sterilizer.Sterilize(source);
					var expected = connectionStringInfo.Masked;
					Assert.AreEqual(expected, actual);
					Debug.WriteLine("--------------------------------------------------------");
					Debug.WriteLine(source);
					Debug.WriteLine(actual);
				}
			}
		}

		[TestMethod]
		public void ShouldRecognizeStandardConnectionStrings()
		{
			foreach (var cs in TestData.ConnectionStrings.StandardConnectionStrings)
			{
				Assert.AreEqual(true, ConnectionStringSterilizer.IsStandardConnectionString(cs), cs);
			}
		}

		[TestMethod]
		public void ShouldNotRecognizeNonConnectionStrings()
		{
			foreach (var cs in TestData.ConnectionStrings.NotStandardConnectionStrings)
			{
				Assert.AreEqual<bool>(false, ConnectionStringSterilizer.IsStandardConnectionString(cs), cs);
			}
		}

		[TestMethod]
		public void ShouldHandleNullConnectionString()
		{
			HandleUnchanged(null);
		}

		[TestMethod]
		public void ShouldHandleEmptyConnectionString()
		{
			HandleUnchanged("");
		}

		[TestMethod]
		public void ShouldHandleRubbishConnectionString()
		{
			HandleUnchanged("asdf");
		}

		private void HandleUnchanged(string connectionString)
		{
			var sterilizer = new ConnectionStringSterilizer(new SterilizerSettings());
			string actual = sterilizer.Sterilize(connectionString);
			Assert.AreEqual(connectionString, actual);
		}
	}
}
