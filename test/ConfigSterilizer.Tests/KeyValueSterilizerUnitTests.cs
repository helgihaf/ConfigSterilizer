﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Marsonsoft.ConfigSterilizer.Tests
{
	[TestClass]
	public class KeyValueSterilizerUnitTests
	{
		[TestMethod]
		public void ShouldCreateKeyValueSterilizer()
		{
			var sterilizer = new KeyValueSterilizer(new SterilizerSettings());
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ShouldThrowExceptionOnNullSettings()
		{
			new KeyValueSterilizer(null);
		}

		[TestMethod]
		public void ShouldKnowBasicPasswordKeys()
		{
			AssertAllAreSecret(TestData.SecretKeys.BasicPasswordKeys);
		}

		[TestMethod]
		public void ShouldKnowCommonPasswordKeys()
		{
			AssertAllAreSecret(TestData.SecretKeys.CommonKeys);
		}

		[TestMethod]
		public void ShouldKnowApiKeys()
		{
			AssertAllAreSecret(TestData.SecretKeys.ApiKeyKeys);
		}

		[TestMethod]
		public void ShouldRejectNonPasswordKeys()
		{
			AssertNoneAreSecret(TestData.SecretKeys.NonPasswordKeys);
		}

		[TestMethod]
		public void ShouldWantMaskOnNonFormatValueWhenFormatStringsAllowed()
		{
			const string Key = "password";
			const string Value = "tiger";
			var sterilizer = new KeyValueSterilizer(new SterilizerSettings());
			bool shouldSterilize = sterilizer.ShouldMaskKeyValue(Key, Value);
			Assert.IsTrue(shouldSterilize);
		}

		[TestMethod]
		public void ShouldNotWantMaskOnFormatValueWhenFormatStringsAllowed()
		{
			const string Key = "password";
			const string Value = "{0}";
			var sterilizer = new KeyValueSterilizer(new SterilizerSettings());
			bool shouldSterilize = sterilizer.ShouldMaskKeyValue(Key, Value);
			Assert.IsFalse(shouldSterilize);
		}

		[TestMethod]
		public void ShouldWantMaskOnNullValueWhenFormatStringsAllowed()
		{
			const string Key = "password";
			const string Value = null;
			var sterilizer = new KeyValueSterilizer(new SterilizerSettings());
			bool shouldSterilize = sterilizer.ShouldMaskKeyValue(Key, Value);
			Assert.IsTrue(shouldSterilize);
		}

		[TestMethod]
		public void ShouldWantMaskOnComplexFormatValueWhenFormatStringsAllowed()
		{
			const string Key = "password";
			const string Value = "{0:10:C}";
			var sterilizer = new KeyValueSterilizer(new SterilizerSettings());
			bool shouldSterilize = sterilizer.ShouldMaskKeyValue(Key, Value);
			Assert.IsTrue(shouldSterilize);
		}

		[TestMethod]
		public void ShouldWantMaskOnNonFormatValueWhenFormatStringsNotAllowed()
		{
			const string Key = "password";
			const string Value = "tiger";
			var settings = new SterilizerSettings();
			settings.AllowSimpleFormatStringInValue = false;
			var sterilizer = new KeyValueSterilizer(settings);
			bool shouldSterilize = sterilizer.ShouldMaskKeyValue(Key, Value);
			Assert.IsTrue(shouldSterilize);
		}

		[TestMethod]
		public void ShouldWantMaskOnFormatValueWhenFormatStringsNotAllowed()
		{
			const string Key = "password";
			const string Value = "{0}";
			var settings = new SterilizerSettings();
			settings.AllowSimpleFormatStringInValue = false;
			var sterilizer = new KeyValueSterilizer(settings);
			bool shouldSterilize = sterilizer.ShouldMaskKeyValue(Key, Value);
			Assert.IsTrue(shouldSterilize);
		}

		[TestMethod]
		public void ShouldSterilizeUsingDefaultMask()
		{
			const string Key = "password";
			const string Value = "tiger";
			var sterilizer = new KeyValueSterilizer(new SterilizerSettings());
			var sterilized = sterilizer.SterilizeKeyValue(Key, Value);
			Assert.AreEqual(SterilizerSettings.DefaultMask, sterilized);
		}

		[TestMethod]
		public void ShouldSterilizeUsingCustomMask()
		{
			const string Key = "password";
			const string Value = "tiger";
			const string CustomMask = "####";
			var settings = new SterilizerSettings();
			settings.Mask = CustomMask;
			var sterilizer = new KeyValueSterilizer(settings);
			var sterilized = sterilizer.SterilizeKeyValue(Key, Value);
			Assert.AreEqual(CustomMask, sterilized);
			Assert.AreNotEqual(CustomMask, SterilizerSettings.DefaultMask);
		}

		[TestMethod]
		public void ShouldSterilizeToOriginalWhenNonPasswordKey()
		{
			const string Key = "User Id";
			const string Value = "scott";
			var sterilizer = new KeyValueSterilizer(new SterilizerSettings());
			var sterilized = sterilizer.SterilizeKeyValue(Key, Value);
			Assert.AreEqual(Value, sterilized);
		}

		[TestMethod]
		public void ShouldTrySterilizeTrueOnPasswordKey()
		{
			const string Key = "password";
			const string Value = "tiger";
			var sterilizer = new KeyValueSterilizer(new SterilizerSettings());
			string sterilizedValue;
			bool sterilized = sterilizer.TrySterilizeKeyValue(Key, Value, out sterilizedValue);
			Assert.IsTrue(sterilized);
			Assert.AreEqual(SterilizerSettings.DefaultMask, sterilizedValue);
		}


		[TestMethod]
		public void ShouldTrySterilizeFalseOnNonPasswordKey()
		{
			const string Key = "username";
			const string Value = "tiger";
			var sterilizer = new KeyValueSterilizer(new SterilizerSettings());
			string sterilizedValue;
			bool sterilized = sterilizer.TrySterilizeKeyValue(Key, Value, out sterilizedValue);
			Assert.IsFalse(sterilized);
		}

		[TestMethod]
		public void ShouldSupportCustomFullKeys()
		{
			var settings = new SterilizerSettings();
			settings.FullKeys.Add("lykilorð");
			AssertSterilization(settings, "lykilorð", "LeyndarMál");
		}

		[TestMethod]
		public void ShouldSupportCustomPartialKeys()
		{
			var settings = new SterilizerSettings();
			settings.PartialKeys.Add("lyk");
			AssertSterilization(settings, "lykilorð", "LeyndarMál");
		}

		[TestMethod]
		public void ShouldSupportCleanKeys()
		{
			const string Key = "password";
			const string Value = "secret";

			var defaultSettings = new SterilizerSettings();
			AssertSterilization(defaultSettings, Key, Value);

			var cleanSettings = new SterilizerSettings();
			cleanSettings.FullKeys.Clear();
			cleanSettings.PartialKeys.Clear();
			AssertNoSterilization(cleanSettings, Key, Value);			
		}

		private static void AssertAllAreSecret(IEnumerable<string> keys)
		{
			var keyValueSterilizer = new KeyValueSterilizer(new SterilizerSettings());
			foreach (string key in keys)
			{
				bool isPasswordKey = keyValueSterilizer.IsSecretKey(key);
				Assert.IsTrue(isPasswordKey, key);
			}
		}

		private static void AssertNoneAreSecret(IEnumerable<string> keys)
		{
			var keyValueSterilizer = new KeyValueSterilizer(new SterilizerSettings());
			foreach (string key in keys)
			{
				bool isPasswordKey = keyValueSterilizer.IsSecretKey(key);
				Assert.IsFalse(isPasswordKey, key);
			}
		}

		private void AssertSterilization(ISterilizerSettings settings, string key, string value)
		{
			var sterilizer = new KeyValueSterilizer(settings);
			string sterilizedValue;
			bool sterilized = sterilizer.TrySterilizeKeyValue(key, value, out sterilizedValue);
			Assert.IsTrue(sterilized);
			Assert.AreEqual(settings.Mask, sterilizedValue);
		}

		private void AssertNoSterilization(ISterilizerSettings settings, string key, string value)
		{
			var sterilizer = new KeyValueSterilizer(settings);
			string sterilizedValue;
			bool sterilized = sterilizer.TrySterilizeKeyValue(key, value, out sterilizedValue);
			Assert.IsFalse(sterilized);
		}

	}
}
