﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Marsonsoft.ConfigSterilizer.Tests
{
	internal static class XUtils
	{

		public static string Normalize(XDocument xdoc)
		{
			return xdoc.ToString(SaveOptions.DisableFormatting);
		}

		internal static string Normalize(string xml)
		{
			return Normalize(XDocument.Parse(xml));
		}

		public static void AssertAreEqual(XDocument expected, XDocument actual)
		{
			string expectedString = Normalize(expected);
			string actualString = Normalize(actual);
			AssertAreEqual(expectedString, actualString);
		}

		internal static string NormalizeFile(string filePath)
		{
			return Normalize(XDocument.Load(filePath));
		}

		public static void AssertAreEqual(string expectedString, string actualString)
		{
			bool areEqual = string.Equals(expectedString, actualString);
			if (!areEqual)
			{
				Debug.WriteLine("Expected/Actual:");
				Debug.WriteLine(expectedString);
				Debug.WriteLine(actualString);
			}
			Assert.IsTrue(areEqual);
		}

	}
}
